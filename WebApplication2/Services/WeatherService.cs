﻿using Npgsql;
using WebApplication2.Models.Data.Weather;
using WebApplication2.Models.Requests;
using WebApplication2.Models.Responses;
using WebApplication2.Services.Interfaces;

namespace WebApplication2.Services
{
    public class WeatherService : IWeatherService
    {
        public async Task<WeatherInitializeResponse> Initialize(WeatherInitializeRequest req)
        {
            using var sr = new StreamReader(req.CsvFileStream);
            var header = sr.ReadLine().Split(req.Delimeter);
            var line = sr.ReadLine();

            var eventTypes = new List<EventType>();
            var severityTypes = new List<SeverityType>();

            string connParam = @"Server=127.0.0.1;Port=5433;User Id=postgres;Password=qwerty12345;Database=web_api_db;";
            var conn = new NpgsqlConnection(connParam);
            var comm = new NpgsqlCommand("TRUNCATE TABLE WeatherEvents;", conn);
            conn.Open();
            await comm.ExecuteScalarAsync();

            comm = new NpgsqlCommand("TRUNCATE TABLE EventTypes;", conn);
            await comm.ExecuteScalarAsync();
            comm = new NpgsqlCommand("TRUNCATE TABLE SeverityTypes;", conn);
            await comm.ExecuteScalarAsync();

            var errorObjectsCount = 0;

            while (line != null)
            {
                var fields = line.Split(req.Delimeter);
                var eventId = fields[0];
                var eventType = fields[1];
                var severityType = fields[2];
                if (!DateTime.TryParse(fields[3], out DateTime startTime))
                {
                    throw new Exception("Csv file has wrong start time format. It must be a datetime.");
                }

                if (!DateTime.TryParse(fields[4], out DateTime endTime))
                {
                    throw new Exception("Csv file has wrong end time format. It must be a datetime.");
                }

                if (!double.TryParse(fields[5].Replace(".", ","), out double precipitation))
                {
                    throw new Exception("Csv file has wrong precipitation format. It must be a double.");
                }

                var timeZone = fields[6];
                var airportCode = fields[7];
                if (!double.TryParse(fields[8].Replace(".", ","), out double locationLatitude))
                {
                    throw new Exception("Csv file has wrong latitude of location format. It must be a double.");
                }

                if (!double.TryParse(fields[9].Replace(".", ","), out double locationLongitude))
                {
                    throw new Exception("Csv file has wrong longitude of location format. It must be a double.");
                }

                var city = fields[10];
                var country = fields[11];
                var state = fields[12];
                var zipCode = fields[13];

                var eventTypeObj = new EventType
                {
                    Id = eventType.Length + 1,
                    Name = eventType
                };
                var severityTypeObj = new SeverityType
                {
                    Id = severityType.Length + 1,
                    Name = severityType
                };

                if (!eventTypes.Exists(et => eventTypeObj.Name == et.Name))
                {
                    eventTypes.Add(eventTypeObj);
                    comm = new NpgsqlCommand(
                        $"INSERT INTO EventTypes (Name) VALUES ('{eventTypeObj.Name}');", conn);
                    await comm.ExecuteScalarAsync();
                }

                if (!severityTypes.Exists(st => severityTypeObj.Name == st.Name))
                {
                    severityTypes.Add(severityTypeObj);
                    comm = new NpgsqlCommand(
                        $"INSERT INTO SeverityTypes (Name) VALUES ('{severityTypeObj.Name}');", conn);
                    await comm.ExecuteScalarAsync();
                }

                var weatherEvent = new WeatherEvent
                {
                    EventId = eventId,
                    EventTypeId = eventTypes.First(et => et.Name == eventType).Id,
                    SeverityTypeId = severityTypes.First(st => st.Name == severityType).Id,
                    StartTime = startTime,
                    EndTime = endTime,
                    Precipitation = precipitation,
                    TimeZone = timeZone,
                    AirportCode = airportCode,
                    LocationLatitude = locationLatitude,
                    LocationLongitude = locationLongitude,
                    City = city,
                    Country = country,
                    State = state,
                    ZipCode = zipCode
                };

                line = sr.ReadLine();
                try
                {
                    comm = new NpgsqlCommand(
                        $"INSERT INTO WeatherEvents (EventId, EventTypeId, SeverityTypeId, StartTime, EndTime, Precipitation, Timezone, AirportCode, LocationLatitude, LocationLongitude, City, Country, State, ZipCode) VALUES {weatherEvent.ToDataBaseString()};",
                        conn);
                    await comm.ExecuteScalarAsync();
                }
                catch (Exception)
                {
                    errorObjectsCount++;
                }
            }

            /*var values = "";
            foreach (var we in weatherEvents)
            {
                values += $"({we.ToDataBaseString()}), ";
            }
            values = values.Trim().Substring(0, values.Length - 2);*/
            await conn.CloseAsync();
            return new WeatherInitializeResponse(true, errorObjectsCount);
        }
    }
}