﻿using WebApplication2.Models.Requests;
using WebApplication2.Models.Responses;

namespace WebApplication2.Services.Interfaces
{
    public interface IWeatherService
    {
        public Task<WeatherInitializeResponse> Initialize(WeatherInitializeRequest req);
    }
}
