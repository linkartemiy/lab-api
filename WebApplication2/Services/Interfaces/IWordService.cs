﻿using WebApplication2.Models.Requests;
using WebApplication2.Models.Responses;

namespace WebApplication2.Services.Interfaces
{
    public interface IWordService
    {
        WordGenerateResponse Generate(WordGenerateRequest req);
    }
}
