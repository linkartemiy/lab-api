﻿using HtmlAgilityPack;
using WebApplication2.Models.Requests;
using WebApplication2.Models.Responses;
using WebApplication2.Services.Interfaces;

namespace WebApplication2.Services
{
    public class UrlService : IUrlService
    {
        public async Task<UrlGetLinksResponse> GetLinksFromWebsite(UrlGetLinksRequest req)
        {
            var client = new HttpClient();
            var response = await client.GetAsync(new Uri(req.Url).AbsoluteUri);
            string responseBody = await response.Content.ReadAsStringAsync();
            var htmlSnippet = new HtmlDocument();
            htmlSnippet.LoadHtml(responseBody);
            var hrefTags = htmlSnippet.DocumentNode
                .SelectNodes("//a[@href]")
                .Select(link => link.Attributes["href"])
                .Select(att => att.Value)
                .ToList();

            return new UrlGetLinksResponse(hrefTags.ToArray(), hrefTags.Count);
        }
    }
}
