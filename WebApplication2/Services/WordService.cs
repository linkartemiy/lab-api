﻿using WebApplication2.Models.Requests;
using WebApplication2.Models.Responses;
using WebApplication2.Services.Interfaces;

namespace WebApplication2.Services
{
    public class WordService : IWordService
    {
        public WordGenerateResponse Generate(WordGenerateRequest req)
        {
            var rnd = new Random();
            var words = new string[req.Length];
            for (int i = 0; i < req.Length; i++)
            {
                var word = "";
                while (word.Length < req.Length)
                {
                    word += req.Letters[rnd.Next(0, req.Letters.Length)];
                }
                words[i] = word;
            }
            return new WordGenerateResponse(words);
        }
    }
}
