﻿namespace WebApplication2.Models.Data.Weather
{
    public record EventType
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
