﻿namespace WebApplication2.Models.Data.Weather
{
    public record WeatherEvent
    {
        public string EventId { get; set; }
        public int EventTypeId { get; set; }
        public int SeverityTypeId { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public double Precipitation { get; set; }
        public string TimeZone { get; set; }
        public string AirportCode { get; set; }
        public double LocationLatitude { get; set; }
        public double LocationLongitude { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }

        public string ToDataBaseString()
        {
            return $"('{EventId}', {EventTypeId}, {SeverityTypeId}, '{StartTime}', '{EndTime}', {Precipitation.ToString().Replace(",", ".")}, '{TimeZone}', '{AirportCode}', {LocationLatitude.ToString().Replace(",", ".")}, {LocationLongitude.ToString().Replace(",", ".")}, '{City}', '{Country}', '{State}', '{ZipCode}')";
        }
    }
}
