﻿namespace WebApplication2.Models.Data.Weather
{
    public record SeverityType
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
