﻿namespace WebApplication2.Models.Responses
{
    public class WeatherInitializeResponse
    {
        public bool Status { get; set; }
        public int ErrorObjects { get; set; }

        public WeatherInitializeResponse(bool status, int errorObjects)
        {
            Status = status;
            ErrorObjects = errorObjects;
        }
    }
}
