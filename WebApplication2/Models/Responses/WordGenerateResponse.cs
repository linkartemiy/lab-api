﻿namespace WebApplication2.Models.Responses
{
    public class WordGenerateResponse
    {
        public string[] Words { get; set; }

        public WordGenerateResponse(string[] words)
        {
            Words = words;
        }
    }
}
