﻿namespace WebApplication2.Models.Responses
{
    public class UrlGetLinksResponse
    {
        public string[] Links { get; set; }
        public int Length { get; set; }

        public UrlGetLinksResponse(string[] links, int length)
        {
            Links = links;
            Length = length;
        }
    }
}
