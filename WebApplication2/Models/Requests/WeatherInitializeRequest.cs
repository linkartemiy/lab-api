﻿namespace WebApplication2.Models.Requests
{
    public class WeatherInitializeRequest
    {
        public Stream CsvFileStream { get; }
        public string Delimeter { get; }

        public WeatherInitializeRequest(Stream csvFileStream, string delimeter)
        {
            CsvFileStream = csvFileStream;
            Delimeter = delimeter;
        }
    }
}
