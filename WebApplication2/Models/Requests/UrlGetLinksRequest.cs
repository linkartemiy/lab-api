﻿namespace WebApplication2.Models.Requests
{
    public class UrlGetLinksRequest
    {
        public string Url { get; }

        public UrlGetLinksRequest(string url)
        {
            Url = url;
        }
    }
}
