﻿namespace WebApplication2.Models.Requests
{
    public class WordGenerateRequest
    {
        public char[] Letters { get; }
        public int Length { get; }

        public WordGenerateRequest(char[] letters, int length)
        {
            Letters = letters;
            Length = length;
        }
    }
}
