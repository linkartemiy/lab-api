﻿using Microsoft.AspNetCore.Mvc;
using WebApplication2.Models.Responses;
using WebApplication2.Services.Interfaces;

namespace WebApplication2.Controllers
{
    /// <summary>
    /// Word controller for word type functions.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class WordController : ControllerBase
    {
        private readonly IWordService _wordService;

        public WordController(IWordService wordService)
        {
            _wordService = wordService;
        }

        /// <summary>
        /// Generates words randomly with letters and length provided.
        /// </summary>
        /// <param name="letters">String of letters to use during generation.</param>
        /// <param name="length">Length of words and letters in them.</param>
        /// <response code="200">Returns words.</response>
        /// <response code="400">All letters must be unique and length must be >= 1</response>
        [HttpGet("{letters}/{length}", Name = "GetWordsByLetters")]
        [ProducesResponseType(typeof(WordGenerateResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status400BadRequest)]
        public IActionResult Generate(string letters, int length)
        {
            if (length < 1)
            {
                return BadRequest("Length must be >= 1");
            }
            var lettersCharArray = letters.ToCharArray();
            if (lettersCharArray.Length != lettersCharArray.Distinct().Count())
            {
                return BadRequest("All letters must be unique.");
            }
            return Ok(_wordService.Generate(new Models.Requests.WordGenerateRequest(lettersCharArray, length)));
        }
    }
}
