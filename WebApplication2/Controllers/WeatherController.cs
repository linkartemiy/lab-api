﻿using Microsoft.AspNetCore.Mvc;
using WebApplication2.Models.Requests;
using WebApplication2.Models.Responses;
using WebApplication2.Services.Interfaces;

namespace WebApplication2.Controllers
{
    /// <summary>
    /// Word controller for word type functions.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class WeatherController : ControllerBase
    {
        private readonly IWeatherService _weatherService;

        public WeatherController(IWeatherService weatherService)
        {
            _weatherService = weatherService;
        }

        /// <summary>
        /// Initializes the weather events tables.
        /// </summary>
        /// <response code="200">Returns words.</response>
        /// <response code="400">Server data handling error.</response>
        [HttpPost("single-file", Name = "InitializeWeatherEventsTables")]
        [RequestFormLimits(ValueLengthLimit = int.MaxValue, MultipartBodyLengthLimit = int.MaxValue)]
        [DisableRequestSizeLimit]
        [ProducesResponseType(typeof(WeatherInitializeResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> InitializeAsync(IFormFile file, string delimeter)
        {
            try
            {
                await using var sr = file.OpenReadStream();
                var res = await _weatherService.Initialize(new WeatherInitializeRequest(sr, delimeter));
                sr.Close();
                return Ok(res);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
